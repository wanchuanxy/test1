package com.example.activitytest;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class FirstActivity extends BaseActivity {

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 1:
                if (resultCode == RESULT_OK){
                    //if里边的resultCode打成requestCode，结果Hello FirstActivity没有返回
                    // 调试了20多分钟，终于发现………………！！！！！！！！！！！
                    String returnedData = data.getStringExtra("data_return");
                    Log.d("FirstActivity",returnedData);
                }
                break;
            default:
        }
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Log.d("FirstActivity", "onRestart");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Log.d("FirstActivity",this.toString());
        Log.d("FirstActivity","Task id is " +getTaskId());

        setContentView(R.layout.first_layout);
       Button button1 = (Button) findViewById(R.id.button_1);
       button1.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v){
//               Toast.makeText(FirstActivity.this, "你戳了一下小白菜",
//                       Toast.LENGTH_SHORT).show();

//               进入第二活动
//               Intent intent = new Intent("com.example.activitytest.ACTION_START");
//               intent.addCategory("com.example.activitytest.MY_CATEGORY");
//               startActivity(intent);

//               打电话
//               Intent intent = new Intent(Intent.ACTION_DIAL);
//               intent.setData(Uri.parse("tel:10086"));
//               startActivity(intent);

//               向下一个活动传输数据
//               String data = "床前明月光";
//               Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
//               intent.putExtra("extra_data", data);
//               startActivity(intent);

//               P.51
//           Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
//           startActivityForResult(intent,1);

//           Intent intent = new Intent(FirstActivity.this, FirstActivity.class);
           Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
           startActivity(intent);

           }
       });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.add_item:
                Toast.makeText(this,"You clicked Add", Toast.LENGTH_SHORT).show();
                break;
            case R.id.remove_item:
                Toast.makeText(this, "You clicked Romove", Toast.LENGTH_SHORT).show();
                break;
            default:
        }
        return true;
    }
}
