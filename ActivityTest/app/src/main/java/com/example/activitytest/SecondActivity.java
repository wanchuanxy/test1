package com.example.activitytest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.EventLogTags;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class SecondActivity extends BaseActivity {

    @Override
    public void onBackPressed() {
        Intent intent  = new Intent();
        intent.putExtra("data_return", "Hello FirstActivity");
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d("SecondActivity", "onDestroy");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Log.d("SecondActivity",this.toString());
        Log.d("SecondActivity","Task id is " +getTaskId());

        setContentView(R.layout.second_layout);
        Button button2 = (Button) findViewById(R.id.button_2);
        button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Intent intent = new Intent (SecondActivity.this, ThirdActivity.class);
                startActivity(intent);
//                Toast.makeText(SecondActivity.this, "你又戳了一下小白菜~",
//                        Toast.LENGTH_SHORT).show();

//                P.51
//                Intent intent = new Intent();
//                intent.putExtra("data_return","Hello FirstActivity");
//                setResult(RESULT_OK,intent);
//                finish();

            }
        });

//        这段代码是调试“向下一个活动传递数据”用的，留着会
//        影响“返回数据给上一个活动”的调试——点击FirstActivity的按钮时会销毁活动
//        Intent intent = getIntent();
//        String data = intent.getStringExtra("extra_data");
//        Log.d("SecondActivity",data);

    }
}
