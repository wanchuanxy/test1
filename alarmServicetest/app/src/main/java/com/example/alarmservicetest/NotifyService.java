package com.example.alarmservicetest;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class NotifyService extends Service {
    public NotifyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onCreate() {
        MainActivity app = MainActivity.getApp();
        app.btEvent("from NotifyService");
        Toast.makeText(this, "这是系统自带的service", Toast.LENGTH_LONG).show();
    }
}
