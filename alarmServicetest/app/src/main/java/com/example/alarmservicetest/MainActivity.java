package com.example.alarmservicetest;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static MainActivity appRef= null;
    private Button b_call_service, b_exit_service;
    boolean k = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appRef = this;
        setContentView(R.layout.activity_main);
        b_call_service = (Button) findViewById(R.id.call_alarm_service);
        b_call_service.setOnClickListener(this);
        b_exit_service = (Button) findViewById(R.id.exit);
        b_exit_service.setOnClickListener(this);
    }

    public static MainActivity getApp() {
        return appRef;
    }

    public void btEvent(String data){
        setTitle(data);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onClick(View arg0){
        if(arg0 == b_call_service){
            setTitle("Waiting... Alarm=5");

            Intent intent = new Intent(MainActivity.this, AlarmRecevier.class);
            PendingIntent p_intent = PendingIntent.getBroadcast(
                    MainActivity.this, 0, intent, 0);

            //用日历对像存储时间（setTime）
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.add(Calendar.SECOND, 3);

            //设置闹钟服务——模式，时间(calendar.getTimeInMillis())，intent
            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
            am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    p_intent);
        }

        if(arg0 == b_exit_service){
            Intent intent = new Intent(MainActivity.this,AlarmRecevier.class);
            PendingIntent p_intent = PendingIntent.getBroadcast(
                    MainActivity.this, 0, intent, 0);
            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
            am.cancel(p_intent);//取消这个跳转
            finish();
        }
    }

}
