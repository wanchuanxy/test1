package com.example.listviewforthepredictor;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by 700 on 2018/3/11.
 */

public class AppinfoAdapter extends ArrayAdapter<Appinfo>{

    private int resourceId;

    public AppinfoAdapter(Context context, int textViewResourceId,
                          List<Appinfo> objects) {
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Appinfo appinfo = getItem(position);
        View view = LayoutInflater.from(getContext()).inflate(resourceId, parent,
                false);
        ImageView Appicon = (ImageView) view.findViewById(R.id.app_icon);
        TextView Appname = (TextView) view.findViewById(R.id.app_name);
        TextView AppTotaltraffic = (TextView) view.findViewById(R.id.app_powerandnet);
        Appicon.setImageDrawable(appinfo.geticon());
        Appname.setText(appinfo.getName());
        AppTotaltraffic.setText(appinfo.getTotaltraffic());
        return view;
    }
}
