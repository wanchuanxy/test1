package com.example.listviewforthepredictor;

import android.graphics.drawable.Drawable;

/**
 * Created by 700 on 2018/3/11.
 */

public class Appinfo {
    //应用名称
    private String name;
    //图标
    private Drawable icon;
    //总流量
    private String Totaltraffic;

    public Appinfo(String name, Drawable icon){
        this.name = name;
        this.icon = icon;
    }

    public Appinfo() {
        super();
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public Drawable geticon(){
        return icon;
    }
    public void seticon(Drawable icon){
        this.icon = icon;
    }
    public String getTotaltraffic() {return Totaltraffic;}
    public void setTotaltraffic(String totaltraffic) {this.Totaltraffic = totaltraffic;}
}
