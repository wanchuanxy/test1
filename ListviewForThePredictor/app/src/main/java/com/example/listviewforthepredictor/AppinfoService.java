package com.example.listviewforthepredictor;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.TrafficStats;
import android.text.format.Formatter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 700 on 2018/3/11.
 */

public class AppinfoService {

    private Context context;
    private PackageManager pm;
    public AppinfoService(Context context){
        this.context = context;
        pm = context.getPackageManager();
    }
    /**
     * 得到手机中所有的应用程序信息
     * &return
     */
    public List<Appinfo> getAppinfos(){
        //创建要返回的集合对象
        List<Appinfo> appinfos = new ArrayList<Appinfo>();
        //获取手机中所有安装的应用集合
        List<ApplicationInfo> applicationInfos = pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);
        //遍历所有的应用集合
        for(ApplicationInfo info : applicationInfos){

            Appinfo appinfo = new Appinfo();

            //获取应用程序的图标
            Drawable icon = info.loadIcon(pm);
            appinfo.seticon(icon);

            //获取应用的名称
            String name = info.loadLabel(pm).toString();
            appinfo.setName(name);
            getAppTrafficList(appinfo);

            appinfos.add(appinfo);
        }
        return appinfos;
    }
    public void getAppTrafficList(Appinfo appinfo){
        //获取所有的安装在手机上的应用软件的信息，并且获取这些软件里面的权限信息
        List<PackageInfo> packageInfos = pm.getInstalledPackages(
                PackageManager.GET_UNINSTALLED_PACKAGES| PackageManager.GET_PERMISSIONS);
        //遍历每个应用包信息
        for(PackageInfo info: packageInfos){
            //请求每个程序包对应的androidManifest.xml里面的权限
            String[] permissions = info.requestedPermissions;
            if(permissions != null && permissions.length > 0){
                //获取每个应用程序在操作系统内的进程id
                int uId = info.applicationInfo.uid;
                //如果返回-1，代表不支持使用该方法，注意必须是2.2以上的
                //获得接收的比特
                long rx = TrafficStats.getUidRxBytes(uId);
                //如果返回-1，代表不支持使用该方法，注意必须是2.2以上的
                //获得发送的比特
                long tx = TrafficStats.getUidTxBytes(uId);
                if(rx < 0 || tx < 0){
                    continue;
                }
                else
                {
                  if(info.applicationInfo.loadLabel(pm).toString() == appinfo.getName()){
                      long total = rx+tx;
                      Long Total = new Long(total);
                      String Totaltraffic = Total.toString();
                      appinfo.setTotaltraffic(Totaltraffic);
                      return;
                  }
                }
            }
                    ;
        }
    }

}






