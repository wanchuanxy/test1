package com.example.listviewforthepredictor;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
     Timer timer=new Timer();       //****
    Handler hendler= new Handler(){
        public void handleMessage(Message mag){
            switch (mag.what){
                case 1:
                    appinfos=appinfoService.getAppinfos();
                    break;
            }
            super.handleMessage(mag);
        }

    };
//    private List<Appinfo> appinfoList = new ArrayList<>();
    AppinfoService appinfoService ;
    List<Appinfo> appinfos ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appinfoService = new AppinfoService(MainActivity.this);
        appinfos = appinfoService.getAppinfos();
        AppinfoAdapter adapter = new AppinfoAdapter(MainActivity.this,
                R.layout.appinfo_item, appinfos);
        ListView listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        timer.schedule(new RTask(),1000,10000);//***
    }
    class RTask extends TimerTask {
        public void run(){
             Message msg=new Message();
            msg.what=1;
            hendler.sendMessage(msg);
        }
    }//***

//    private void initAppinfo(){
//        for(int i = 0; i < 2; i++){
//            Appinfo Google = new Appinfo("Google",R.mipmap.ic_launcher);
//            appinfoList.add(Google);
//            Appinfo Wechat = new Appinfo("Wechat",R.mipmap.ic_launcher);
//            appinfoList.add(Wechat);
//            Appinfo QQ = new Appinfo("QQ",R.mipmap.ic_launcher);
//            appinfoList.add(QQ);
//            Appinfo JD = new Appinfo("JD",R.mipmap.ic_launcher);
//            appinfoList.add(JD);
//            Appinfo Kugou = new Appinfo("Kugou",R.mipmap.ic_launcher);
//            appinfoList.add(Kugou);
//            Appinfo Taobao = new Appinfo("Taobao",R.mipmap.ic_launcher);
//            appinfoList.add(Taobao);
//            Appinfo NetEase_cloud_music = new Appinfo("NetEase_cloud_music",R.mipmap.ic_launcher);
//            appinfoList.add(NetEase_cloud_music);
//            Appinfo didi = new Appinfo("didi",R.mipmap.ic_launcher);
//            appinfoList.add(didi);
//            Appinfo microblog = new Appinfo("microblog",R.mipmap.ic_launcher);
//            appinfoList.add(microblog);
//        }
//    }
}
