package com.example.graceplayer;

import android.app.Activity;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.data.Music;
import com.example.data.MusicList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity {

    private ImageButton imgBtn_Previous;
    private ImageButton imgBtn_PlayOrPause;
    private ImageButton imgBtn_Stop;
    private ImageButton imgBtn_Next;
    private ListView list;

    //歌曲列表对象
    private ArrayList<Music> musicArrayList;

    private int number = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
        registerListeners();
        initMusicList();
        initListView();
        checkMusicfile();
    }

    private void findViews(){
        imgBtn_Previous = (ImageButton) findViewById(R.id.imageButton1);
        imgBtn_PlayOrPause = (ImageButton) findViewById(R.id.imageButton2);
        imgBtn_Stop= (ImageButton) findViewById(R.id.imageButton3);
        imgBtn_Next = (ImageButton) findViewById(R.id.imageButton4);
        list = (ListView) findViewById(R.id.listView1);
    }

    private void registerListeners(){
        imgBtn_Previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveNumberToPrevious();
                play(number);
                imgBtn_PlayOrPause.setBackgroundResource(R.drawable.button_pause);
            }
        });
        imgBtn_PlayOrPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        imgBtn_Stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        imgBtn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView<?>parent, View view,
                                    int position, long id){

            }
        });
    }

    private void initMusicList(){
        musicArrayList = MusicList.getMusicList();

        if(musicArrayList.isEmpty()){
            Cursor mMusicCursor = this.getContentResolver().query(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    new String[] {MediaStore.Audio.Media.TITLE,
                            MediaStore.Audio.Media.DURATION,
                            MediaStore.Audio.Media.ALBUM,
                            MediaStore.Audio.Media.ARTIST,
                            MediaStore.Audio.Media._ID,
                            MediaStore.Audio.Media.DATA,
                            MediaStore.Audio.Media.DISPLAY_NAME},null, null,
                    MediaStore.Audio.AudioColumns.TITLE);

            if(mMusicCursor != null){
                int indexTitle = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE);
                int indexArtist = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST);
                int indexTotalTime = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION);
                int indexPath = mMusicCursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);

                for(mMusicCursor.moveToFirst();!mMusicCursor.isAfterLast();mMusicCursor.moveToNext()){
                    String strTitle = mMusicCursor.getString(indexTitle);
                    String strArtist = mMusicCursor.getString(indexArtist);
                    String strTotoalTime = mMusicCursor.getString(indexTotalTime);
                    String strPath = mMusicCursor.getString(indexPath);

                    if(strArtist.equals("<unknown>"))
                        strArtist = "无艺术家";
                    Music music = new Music(strTitle, strArtist, strPath, strTotoalTime);
                    musicArrayList.add(music);
                }
            }
        }
    }

    private void initListView(){
        List<Map<String, String>>list_map = new ArrayList<Map<String, String>>();
        HashMap<String, String>map;
        SimpleAdapter simpleAdapter;
        for(Music music : musicArrayList){
            map = new HashMap<String, String>();
            map.put("musicName", music.getmusicName());
            map.put("musicArtist", music.getmusicArtist());
            list_map.add(map);
        }

        String[] from = new String[]{"musicName", "musicArtist"};
        int[] to = {R.id.listview_tv_title_item,R.id.listview_tv_artist_item};

        simpleAdapter = new SimpleAdapter(this, list_map, R.layout.listview, from, to);
        list.setAdapter(simpleAdapter);
    }

    private void checkMusicfile(){
        if(musicArrayList.isEmpty()){
            imgBtn_Next.setEnabled(false);
            imgBtn_PlayOrPause.setEnabled(false);
            imgBtn_Previous.setEnabled(false);
            imgBtn_Stop.setEnabled(false);
            Toast.makeText(getApplicationContext(),"当前没有歌曲文件",Toast.LENGTH_SHORT).show();
        }else{
            imgBtn_Next.setEnabled(true);
            imgBtn_PlayOrPause.setEnabled(true);
            imgBtn_Previous.setEnabled(true);
            imgBtn_Stop.setEnabled(true);
        }
    }

    // 媒体播放类
    private MediaPlayer player = new MediaPlayer();


    /** 读取音乐文件 */
    private void load(int number) {
        try {
            player.reset();
            player.setDataSource(MusicList.getMusicList().get(number).getmusicPath());
            player.prepare();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /** 播放音乐 */
    private void play(int number) {
        // ֹͣ停止当前播放
        if (player != null && player.isPlaying()) {
            player.stop();
        }
        load(number);
        player.start();
    }
    /** 暂停音乐*/
    private void pause() {
        if (player.isPlaying()) {
            player.pause();
        }
    }
    /** 停止播放*/
    private void stop() {
        player.stop();
    }
    /** 恢复播放（暂停之后）*/
    private void resume() {
        player.start();
    }
    /** 重新播放（播放完成之后）*/
    private void replay() {
        player.start();
    }

    /** 选择下一曲 */
    private void moveNumberToNext() {
        // 判断是否到达列表底端
        if ((number ) == MusicList.getMusicList().size()-1) {
            Toast.makeText(MainActivity.this,"已经到达列表底端，现在播放第一曲",Toast.LENGTH_SHORT).show();
        } else {
            ++number;
            play(number);
        }
    }

    /** 选择上一曲*/
    private void moveNumberToPrevious() {
        // 判断是否到达列表顶端
        if (number == 0) {
            Toast.makeText(MainActivity.this,"已经到达列表顶端，现在播放最后一曲",Toast.LENGTH_SHORT).show();
        } else {
            --number;
            play(number);
        }
    }
}



