package com.example.greendaotest;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import lhd.test.hello.DaoSession;
import lhd.test.hello.DaoMaster;//注意自行添加！！！！！

// 我们将DaoSession对象放在myapplication里，避免多次生成 Session 对象，
// 这样全局就使用同一个session链接到数据库啦
/**
 * Created by 700 on 2018/6/7.
 */

public class myapplication extends Application{
    //建立全局的daoSession
    public static DaoSession daoSession;
    public static SQLiteDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();
        setupDatabase();
    }
    private void setupDatabase() {
        // 通过 DaoMaster 的内部类 DevOpenHelper，你可以得到一个便利的 SQLiteOpenHelper 对象。

        // 可能你已经注意到了，你并不需要去编写「CREATE TABLE」这样的 SQL 语句，因为 greenDAO 已经帮你做了。

        // 注意：默认的 DaoMaster.DevOpenHelper 会在数据库升级时，删除所有的表，意味着这将导致数据的丢失。

        // 所以，在正式的项目中，你还应该做一层封装，来实现数据库的安全升级。
        //数据库的名字是my_data
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "my_data", null);
        db = helper.getWritableDatabase();  //操作这个数据库的对象db

        // 注意：该数据库连接属于 DaoMaster（db->daoMaster），所以多个 Session 指的是相同的数据库连接。
        DaoMaster daoMaster = new DaoMaster(db);
        //db->daoMaster,daoMaster建立会话（db<---->sqlite）
        daoSession = daoMaster.newSession();
    }
}
