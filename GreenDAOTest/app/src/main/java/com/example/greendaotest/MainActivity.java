package com.example.greendaotest;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.greenrobot.dao.query.Query;
import lhd.test.hello.MyData;
import lhd.test.hello.MyDataDao;

public class MainActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private EditText edit_input;
    private Cursor cursor;

    private String str_input;

    private MyDataDao dataDao;

    //定义一个全局变量i 用来模拟年龄
    private static int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_input = (EditText) findViewById(R.id.edit_input);//加载组建

        //获取全局的daoSession然后通过daoSession获取mydataDao
        dataDao = myapplication.daoSession.getMyDataDao();
        db = myapplication.db;

        //获取name字段一列的数据
        String textColumn = MyDataDao.Properties.Name.columnName;
        //按本地语言进行排序
        String orderBy = textColumn + " COLLATE LOCALIZED ASC";
        //执行查询返回一个cursor，整张表就拉下来了，放在cursor中
        cursor = db.query(dataDao.getTablename(), dataDao.getAllColumns(), null, null, null, null, orderBy);

        //SimpleCursorAdapter的使用
        String[] from = {textColumn, MyDataDao.Properties.Age.columnName};
        int[] to = {android.R.id.text1, android.R.id.text2};
//        SimpleCursorAdapter的五个参数：
//        context: 上下文
//        data: 数据源， 类型为Map组成的List集合，每一个Map对应ListView中的一个Item，Map中的所有String类型的key组成的一个字符串数组就是参数from，key的作用就是与item关联起来。
//        resource: item的布局文件，其中至少要有参数to数组中的所有视图,这里为simple_list_item_2
//        from: 将被添加到与每个Item相关联的映射的列名称列表，data参数中Map的String类型键名组成的字符串数组
//        to: 显示与from数组对应数据的视图ID，这些视图都存在于resource指定的Item布局文件中
//        关于simple_list_item_2，见https://www.jianshu.com/p/98ece662b6c8
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, from,
                to);    //from放入to，to放入item,item->listview

        ListView listview = (ListView) findViewById(R.id.list);
        listview.setAdapter(adapter);
        //删除
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //删除操作,通过id删除
                dataDao.deleteByKey(id);
                cursor.requery();//重新查数据，拉表（新表置入listView，刷新布局）
            }
        });

    }

    public void onMyClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                add_data();
                break;
            case R.id.search:
                search_data();
                break;
        }
    }

    //添加一条数据
    public void add_data() {
        str_input = edit_input.getText().toString();
        edit_input.setText("");

        //1、创建对象
        MyData data = new MyData();
        String name = str_input;
//        String age = "20" + i++;
        long timeMillis = System.currentTimeMillis();
        Date time = new Date(timeMillis);
        SimpleDateFormat df = new SimpleDateFormat("yyyy'年'MM'月'dd'日'  HH':'mm':'ss a");
        String dateString = df.format(time);
        String age = dateString;



        String cool = "yes";

        //2、设置数据
        data.setName(name);
        data.setAge(age);
        data.setCool(cool);

        //3、插入数据
        dataDao.insert(data);
        cursor.requery();//重新查数据，拉表（新表置入listView，刷新布局）
    }

    //查询
    public void search_data() {
        str_input = edit_input.getText().toString();

        if (edit_input.getText().toString().isEmpty()) {
            Toast.makeText(MainActivity.this, "输入错误", Toast.LENGTH_SHORT).show();
        } else {
            Query query = dataDao.queryBuilder()
                    .where(MyDataDao.Properties.Name.eq(str_input))
                    .orderAsc(MyDataDao.Properties.Age)
                    .build();
            List ls = query.list();   //查询返回的list
            Toast.makeText(MainActivity.this, "有" + ls.size() + "条数据符合", Toast.LENGTH_SHORT).show();
            edit_input.setText("");
        }
    }
}
