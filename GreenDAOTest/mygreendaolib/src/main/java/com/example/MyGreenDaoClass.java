package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyGreenDaoClass {
    public static void main(String[] args) throws Exception {

        Schema schema = new Schema(1,"lhd.test.hello");

        mydatabase(schema);

        new DaoGenerator().generateAll(schema,"D:\\Android\\Test\\test1\\GreenDAOTest\\app\\src\\main\\java-gen");

    }
    private static void mydatabase(Schema schema){

        Entity mydata = schema.addEntity("MyData");


        mydata.addIdProperty();
        mydata.addStringProperty("name");
        mydata.addStringProperty("age");
        mydata.addStringProperty("cool").notNull();
    }
}
