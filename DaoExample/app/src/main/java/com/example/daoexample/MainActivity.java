package com.example.daoexample;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.greenrobot.dao.query.Query;
import ptest.Note;
import ptest.NoteDao;

public class MainActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private EditText edit_input;
    private Cursor cursor;

    private String str_input;

    private NoteDao dataDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_input = (EditText) findViewById(R.id.edit_input);//加载组件
        dataDao = myapplication.daoSession.getNoteDao();
        db = myapplication.db;
        String textColumn = NoteDao.Properties.Text.columnName;

        //按本地语言进行排序
        String orderBy = textColumn + " COLLATE LOCALIZED ASC";
        //执行查询返回一个cursor，整张表就拉下来了，放在cursor中
        cursor = db.query(dataDao.getTablename(), dataDao.getAllColumns(), null, null, null, null, orderBy);

        //SimpleCursorAdapter的使用
        String[] from = {textColumn, NoteDao.Properties.Comment.columnName};//text字段一列的所有值，comment字段一列的所有值
        int[] to = {android.R.id.text1, android.R.id.text2};
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, from,
                to);

        ListView listview = (ListView) findViewById(R.id.list);
        listview.setAdapter(adapter);

//        ArrayAdapter adapter1 = new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line,cursor);

        //删除
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //删除操作,通过id删除
                dataDao.deleteByKey(id);
                cursor.requery();//重新查数据，拉表（新表置入listView，刷新布局）
            }
        });

    }

    public void onMyClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                add_data();
                break;
        }
    }

    //添加一条数据
    public void add_data() {
        str_input = edit_input.getText().toString();
        edit_input.setText("");

        //1、创建对象
        Note data = new Note();
        String text = str_input;
        long timeMillis = System.currentTimeMillis();
        Date time = new Date(timeMillis);
        SimpleDateFormat df = new SimpleDateFormat("yyyy'年'MM'月'dd'日'  HH':'mm':'ss a");
        String dateString = df.format(time);
        String comment = dateString;

        //2、设置数据
        data.setText(text);
        data.setComment(comment);
        data.setDate(time);

        //3、插入数据
        dataDao.insert(data);
        cursor.requery();//重新查数据，拉表（新表置入listView，刷新布局）
    }

//    public void refreshautolist(Cursor cursor){
//        int i=0;
//            if(cursor.moveToFirst()){
//                String name[0]
//            }
//    }
// 实现查询功能：
//遍历cursor，每一个迭代都往动态数组里添加新元素，直至循环完毕
//建立一个动态数组，每次更新cursor的时候，方法中先！！！置空数组！！！，再循环取值
// 取值方法：string text[] = cursor.getString(cursor.getColumnIndex("text"))




}

