package com.example.daoexample;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import ptest.DaoMaster;
import ptest.DaoSession;

/**
 * Created by 700 on 2018/6/7.
 */

public class myapplication extends Application {
    //建立全局的daoSession
    public static DaoSession daoSession;
    public static SQLiteDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();
        setupDatabase();
    }
    private void setupDatabase(){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "note", null);
        db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

}
